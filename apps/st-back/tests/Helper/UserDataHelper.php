<?php

namespace App\Tests\Helper;

class UserDataHelper {
	public static function getAdminToken() {
		return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJiY2U0NmJmMC1hNjVhLTQwM2ItYjNjMS0xNGI4OWY2MmFhZDgiLCJuYW1lIjoic2FtaXIiLCJtYWlsIjoic2FtaXJAc2Ftc29mdC5lcyIsImlzQWRtIjoxLCJpYXQiOjE1ODMzMTQwMDQsImV4cCI6MTU4MzU3MzIwNH0.G94SJx_OJBFU9HIbZNCr7DBikUG1h1amORgQnlQhLdc';
	}

	public static function getLoggedToken() {
		return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3YWVhMjAyNC0wMGI2LTRhOGMtOTgwZC0zZDc3Y2E3ODM0NmYiLCJuYW1lIjoiZWZpIiwibWFpbCI6ImVmaUBzYW1zb2Z0LmVzIiwiaXNBZG0iOjAsImlhdCI6MTU4MzMxMzQ1NywiZXhwIjoxNTgzNTcyNjU3fQ.tkPzKJzcJlwilxEYf9QfkrdH_kc-UhKI-3jus-NllEs';
	}

	public static function getInvalidToken() {
		return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJiYWRhN2VmMi01NzQwLTQ2OGMtYThkOC0zNGEyODc2NzVhOTkiLCJuYW1lIjoiU2FtaXJycnIiLCJtYWlsI';
	}

	public  static function getAdminUuid() {
		return 'bce46bf0-a65a-403b-b3c1-14b89f62aad8';
	}

	public  static function getLoggedUuid() {
		return '7aea2024-00b6-4a8c-980d-3d77ca78346f';
	}

	public  static function getNotFoundUuid() {
		return '3e797a61-fa33-4493-8fcf-a0eda8c010f0';
	}

	public static function getUserNameFound() {
		return 'samir';
	}
}
