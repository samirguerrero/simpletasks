<?php
namespace App\Tests\Controller;

use App\Tests\Helper\ProjectDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProjectControllerTest extends WebTestCase {

	public function testCreateProjectOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/project/create',[
			'json' => json_encode([
				'title' => 'ProjectTest1',
				'summary' => 'Summary',
				'description' => 'Description',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateProjectInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('POST', '/api/v1/project/create',[
			'json' => json_encode([
				'title' => 'ProjectTest1',
				'summary' => 'Summary',
				'description' => 'Description',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateProjectInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/project/create');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateProjectNoTitleProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/project/create',[
			'json' => json_encode([
				'title' => '',
				'summary' => 'Summary',
				'description' => 'Description',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowProjectOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/project/show/' . ProjectDataHelper::getProjectUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowProjectInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/project/show/' . ProjectDataHelper::getProjectUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowProjectNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/project/show/' . ProjectDataHelper::getProjectUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testListProjectOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/project/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListProjectInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/project/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditProjectOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/project/edit/' . ProjectDataHelper::getProjectUuid(),
			[
				'json' => json_encode([
					'title' => 'ProjectTest1',
					'summary' => 'Summary',
					'description' => 'Description',
					'color' => '#FF0'
				])
			]
		);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditProjectInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/project/edit/' . ProjectDataHelper::getProjectUuid(),
			[
			'json' => json_encode([
				'title' => 'ProjectTest1',
				'summary' => 'Summary',
				'description' => 'Description',
				'color' => '#FFF'
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditProjectInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/project/edit/' . ProjectDataHelper::getProjectUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditProjectNoUuidProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT', '/api/v1/project/edit/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditProjectNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/project/edit/' . ProjectDataHelper::getProjectUuidNotFound(),
			[
				'json' => json_encode([
					'title' => 'ProjectTest1',
					'summary' => 'Summary',
					'description' => 'Description',
					'color' => '#FFF'
				])
			]
		);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testEditProjectNothingProvidedOrAreTheSame() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/project/edit/' . ProjectDataHelper::getProjectUuid(),
			[
				'json' => json_encode([
					'title' => '',
					'summary' => '',
					'description' => '',
					'color' => ''
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveProjectOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/project/remove/' . ProjectDataHelper::getProjectUuidWithoutTasks());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveProjectInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/project/remove/' . ProjectDataHelper::getProjectUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveProjectNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/project/remove/' . ProjectDataHelper::getProjectUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}
}
