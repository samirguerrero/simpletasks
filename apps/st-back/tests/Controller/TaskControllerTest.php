<?php
namespace App\Tests\Controller;

use App\Tests\Helper\ColumnDataHelper;
use App\Tests\Helper\ProjectDataHelper;
use App\Tests\Helper\TagDataHelper;
use App\Tests\Helper\TaskDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase {
	
	public function testCreateTaskOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/task/create',[
			'json' => json_encode([
				'title' => 'TaskTest1',
				'summary' => 'Summary',
				'description' => 'Description',
				'attached' => 'attached.jpg',
				'expiration_date' => '10/10/2020',
				'priority' => '0',
				'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
				'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
				'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
				'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateTaskOkWithNewTag() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/task/create',[
			'json' => json_encode([
				'title' => 'TaskTest1',
				'summary' => 'Summary',
				'description' => 'Description',
				'attached' => 'attached.jpg',
				'expiration_date' => '10/10/2020',
				'priority' => '0',
				'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
				'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
				'tag' => ['title' => 'test', 'color' => '#FFF'],
				'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateTaskInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('POST', '/api/v1/task/create',[
			'json' => json_encode([
				'title' => 'TaskTest1',
				'summary' => 'Summary',
				'description' => 'Description',
				'attached' => 'attached.jpg',
				'expiration_date' => '10/10/2020',
				'priority' => '0',
				'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
				'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
				'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
				'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testListByColumnOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET', '/api/v1/task/list-column/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListByColumnInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET', '/api/v1/task/list-column/' . ColumnDataHelper::getColumnUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateTaskNoJsonFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/task/create',[]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateTaskTitleNotProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/task/create',[
			'json' => json_encode([
				'title' => '',
				'summary' => 'Summary',
				'description' => 'Description',
				'attached' => 'attached.jpg',
				'expiration_date' => '10/10/2020',
				'priority' => '0',
				'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
				'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
				'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
				'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateTaskInvalidDate() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/task/create',[
			'json' => json_encode([
				'title' => 'TestTask',
				'summary' => 'Summary',
				'description' => 'Description',
				'attached' => 'attached.jpg',
				'expiration_date' => '10102020',
				'priority' => '0',
				'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
				'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
				'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
				'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowTaskOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/task/show/' . TaskDataHelper::getTaskUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowTaskInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/task/show/' . TaskDataHelper::getTaskUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowTaskNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/task/show/' . TaskDataHelper::getTaskUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testListTaskOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/task/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListTaskInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/task/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/task/edit/' . TaskDataHelper::getTaskUuid(),[
			'json' => json_encode([
				'title' => 'TaskTest1',
				'summary' => 'Summary Test',
				'description' => 'Description Test',
				'attached' => 'attached_test.jpg',
				'expiration_date' => '10/10/2020',
				'priority' => '2',
				'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
				'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
				'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
				'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/task/edit/' . TaskDataHelper::getTaskUuid(),[
				'json' => json_encode([
					'title' => 'TaskTest1',
					'summary' => 'Summary',
					'description' => 'Description',
					'attached' => 'attached.jpg',
					'expiration_date' => '10/10/2020',
					'priority' => '0',
					'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
					'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
					'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
					'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
				])
			]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskNoUuidProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT','/api/v1/task/edit/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskNoJsonProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/task/edit/' . TaskDataHelper::getTaskUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/task/edit/' . TaskDataHelper::getTaskUuidNotFound(),[
				'json' => json_encode([
					'title' => 'TaskTest1',
					'summary' => 'Summary',
					'description' => 'Description',
					'attached' => 'attached.jpg',
					'expiration_date' => '10/10/2020',
					'priority' => '0',
					'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
					'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
					'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
					'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
				])]);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskIncorrectDateFormat() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/task/edit/' . TaskDataHelper::getTaskUuid(),[
				'json' => json_encode([
					'title' => 'TaskTest1',
					'summary' => 'Summary',
					'description' => 'Description',
					'attached' => 'attached.jpg',
					'expiration_date' => '10102020',
					'priority' => '0',
					'assignment' => ['uuid' => UserDataHelper::getLoggedUuid()],
					'project' => ['uuid' => ProjectDataHelper::getProjectUuid()],
					'tag' => ['uuid' => TagDataHelper::getTagUuid(), 'title' => 'test'],
					'column' => ['uuid' => ColumnDataHelper::getColumnUuid()]
				])]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTaskNothingToPersist() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/task/edit/' . TaskDataHelper::getTaskUuid(),[
				'json' => json_encode([
					'title' => '',
					'summary' => '',
					'description' => '',
					'attached' => '',
					'expiration_date' => '',
					'priority' => '',
					'assignment' => '',
					'project' => '',
					'tag' => '',
					'column' => ''
				])]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveTaskOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/task/remove/' . TaskDataHelper::getTaskUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveTaskInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/task/remove/' . TaskDataHelper::getTaskUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveTaskNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/task/remove/' . TaskDataHelper::getTaskUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}
}
