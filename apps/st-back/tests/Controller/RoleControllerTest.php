<?php
namespace App\Tests\Controller;

use App\Tests\Helper\RoleDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoleControllerTest extends WebTestCase {

	public function testCreateRoleOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/role/create',[
			'json' => json_encode([
				'name' => 'RoleTest1',
				'isAdmin' => 0
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateRoleNameNotProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/role/create',[
			'json' => json_encode([
				'name' => '',
				'isAdmin' => 0
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateRoleNameTaken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/role/create',[
			'json' => json_encode([
				'name' => 'Admin',
				'isAdmin' => 1
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateRoleInvalidToken() {

		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('POST', '/api/v1/role/create',[
			'json' => json_encode([
				'name' => 'RoleTest1',
				'isAdmin' => 0
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateRoleInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/role/create');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateRoleNoTitleProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/role/create',[
			'json' => json_encode([
				'title' => '',
				'icon' => 'icon.jpg'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateRoleNoIconProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/role/create',[
			'json' => json_encode([
				'title' => 'title1',
				'icon' => ''
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowRoleOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/role/show/' . RoleDataHelper::getRoleUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowRoleInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/role/show/' . RoleDataHelper::getRoleUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowRoleNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/role/show/' . RoleDataHelper::getRoleUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testListRoleOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/role/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListRoleInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/role/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditRoleOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/role/edit/' . RoleDataHelper::getRoleUuid(),
			[
				'json' => json_encode([
					'name' => 'Admins',
					'isAdmin' => 1
				])
			]
		);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditRoleInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/role/edit/' . RoleDataHelper::getRoleUuid(),
			[
			'json' => json_encode([
				'title' => 'RoleTest1',
				'icon' => 'icon.jpg'
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditRoleInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/role/edit/' . RoleDataHelper::getRoleUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditRoleNoUuidProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT', '/api/v1/role/edit/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditRoleNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/role/edit/' . RoleDataHelper::getRoleUuidNotFound(),
			[
				'json' => json_encode([
					'title' => 'RoleTest1',
					'icon' => 'icon.jpg'
				])
			]
		);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testEditRoleNothingProvided() {

		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/role/edit/' . RoleDataHelper::getRoleUuid(),
			[
				'json' => json_encode([
					'name' => null,
					'isAdmin' => null
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveRoleOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/role/remove/' . RoleDataHelper::getRoleUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveRoleInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/role/remove/' . RoleDataHelper::getRoleUuid());

		$this->assertEquals(403, $client->getResponse()->getStatusCode());
	}

	public function testRemoveRoleNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/role/remove/' . RoleDataHelper::getRoleUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}
}
