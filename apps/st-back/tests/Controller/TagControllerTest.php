<?php
namespace App\Tests\Controller;

use App\Tests\Helper\TagDataHelper;
use App\Tests\Helper\UserDataHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TagControllerTest extends WebTestCase {

	public function testCreateTagOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/tag/create',[
			'json' => json_encode([
				'title' => 'TagTest1',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testCreateTagInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('POST', '/api/v1/tag/create',[
			'json' => json_encode([
				'title' => 'TagTest1',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateTagInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/tag/create');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testCreateTagNoTitleProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('POST', '/api/v1/tag/create',[
			'json' => json_encode([
				'title' => '',
				'color' => '#FFF'
			])
		]);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowTagOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/tag/show/' . TagDataHelper::getTagUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testShowTagInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/tag/show/' . TagDataHelper::getTagUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testShowTagNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/tag/show/' . TagDataHelper::getTagUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testListTagOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/tag/list');

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testListTagInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('GET',
			'/api/v1/tag/list');

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTagOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/tag/edit/' . TagDataHelper::getTagUuid(),
			[
				'json' => json_encode([
					'title' => 'TagTest1',
					'color' => '#FF0'
				])
			]
		);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testEditTagInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('PUT',
			'/api/v1/tag/edit/' . TagDataHelper::getTagUuid(),
			[
			'json' => json_encode([
				'title' => 'TagTest1',
				'color' => '#FFF'
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTagInvalidJson() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/tag/edit/' . TagDataHelper::getTagUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testEditTagNoUuidProvided() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT', '/api/v1/tag/edit/ ');

		$this->assertEquals(500, $client->getResponse()->getStatusCode());
	}

	public function testEditTagNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/tag/edit/' . TagDataHelper::getTagUuidNotFound(),
			[
				'json' => json_encode([
					'title' => 'TagTest1',
					'color' => '#FFF'
				])
			]
		);

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testEditTagNothingProvidedOrAreTheSame() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('PUT',
			'/api/v1/tag/edit/' . TagDataHelper::getTagUuid(),
			[
				'json' => json_encode([
					'title' => '',
					'color' => ''
				])
			]
		);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveTagOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/tag/remove/' . TagDataHelper::getTagUuid());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

	public function testRemoveTagInvalidToken() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getInvalidToken());
		$client->request('DELETE',
			'/api/v1/tag/remove/' . TagDataHelper::getTagUuid());

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	public function testRemoveTagNotFound() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('DELETE',
			'/api/v1/tag/remove/' . TagDataHelper::getTagUuidNotFound());

		$this->assertEquals(404, $client->getResponse()->getStatusCode());
	}

	public function testSearchByTitleOk() {
		$client = static::createClient();
		$client->setServerParameter('HTTP_Auth', UserDataHelper::getAdminToken());
		$client->request('GET',
			'/api/v1/tag/findbytitle/' . TagDataHelper::getTagTitleFound());

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}
}
