#!/bin/bash
echo '///////////////////////////////////'
echo '////////     ////////     /////////'
echo '//////  /////  // // /////  ///////'
echo '////////     ///////     //////////'
echo '///////////////////////////////////'
echo ''
echo ''
echo '********reset db*********'
echo ''
echo ''
echo '--fixture load reset the db and load initial data not recommended for production--'
cd ~/simpletasks-full/apps/st-back
bin/console d:f:l -n
echo ''
echo ''
echo '*********Remove uploads*********'
echo ''
echo ''
rm public/uploads/*
echo '********finish reset db*********'
