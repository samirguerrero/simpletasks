<?php /** @noinspection PhpPossiblePolymorphicInvocationInspection */

namespace App\Controller;

use App\Helper\Errors;
use App\Service\FileUploader;
use App\Service\JwtAuth;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends AbstractController {

	/**
	 * Upload files with token
	 *
	 * @param Request $request
	 * @param FileUploader $uploader
	 * @param JwtAuth $jwtAuth
	 *
	 * @return JsonResponse
	 */
  public function uploadSecured(Request $request, FileUploader $uploader, JwtAuth $jwtAuth) {
    // get token
    $token = $request->headers->get('Auth');

    // check token validity
    if (!$jwtAuth->checkValidToken($token)) {
	    return $this->json(Errors::invalidTokenError(), 400);
    }

    return $this->uploadFile($request, $uploader);
	}

	/**
	 * Upload files without token
	 *
	 * @param Request $request
	 * @param FileUploader $uploader
	 *
	 * @return array|JsonResponse
	 */
	public function uploadUnsecured(Request $request, FileUploader $uploader) {

		$origin = $request->server->get('HTTP_ORIGIN');
		$envOrigin = $_ENV['ALLOWED_ORIGIN'];

		if (!$origin || $origin != $envOrigin) {
			return  $this->json(Errors::notAllowedOrigin(), 403);
		}

		return $this->json($this->uploadFile($request, $uploader));
	}

	/**
	 * Upload a file
	 *
	 * @param Request $request
	 * @param FileUploader $uploader
	 *
	 * @return array|JsonResponse
	 */
	private function uploadFile(Request $request, FileUploader $uploader) {
		// get file from request
		/** @var File $file */
		$file = $request->files->get('uploadFile');

		if (empty($file)) {
			return $this->json(Errors::fileEmptyError(), 400);
		}

		// check mime type
		$fileMime = $file->getMimeType();
		$allowedMimes = [
			'image/jpeg',
			'image/png',
			'application/zip'
		];

		if (!in_array($fileMime, $allowedMimes)) {
			return  $this->json(Errors::notAllowedMimeType(), 400);
		}

		// get file data
		$filename = $file->getClientOriginalName();
		$fileUploadedPath = NULL;
		$basePath = $this->getParameter('kernel.project_dir');
		$uploadDir = $basePath . '/public/uploads';

		// try to move to uploads folder
		try {
			$fileUploadedPath = $uploader->moveFile($uploadDir, $filename, $file);
		} catch (FileException $e) {
			return $this->json(Errors::fileError(), 500);
		}

		// if there are some error then send it
		if (!$fileUploadedPath) {
			return $this->json(Errors::fileError(), 500);
		}

		// a bit of make up to clean path
		$fileUploadedPath =
			str_replace($basePath . '/public','',$fileUploadedPath);

		return [
			'status'  => 'success',
			'code'    => 200,
			'message' => $fileUploadedPath
		];
	}
}
