<?php

namespace App\Controller;


use App\Entity\Role;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\RoleRepository;
use App\Service\AdminChecker;
use App\Service\JwtAuth;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class RoleController extends AbstractController {

	/** @var JsonTools */
	private $jsonTools;

	public function __construct() {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Create a role, only admins can do this action
	 *
	 * @param Request $request  request
	 *
	 * @param AdminChecker $adminChecker admin checker service
	 *
	 * @return JsonResponse response
	 */
	public function create(Request $request, AdminChecker $adminChecker) {
		// get token
		$token = $request->headers->get('Auth');

		// check if is an admin
		$isAdminResult = $adminChecker->isAdmin($token);

		// if is an array then an error happened
		if (is_array($isAdminResult)) {
			$code = $isAdminResult['code'];
			return $this->json($isAdminResult, $code);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$isAdminRole = $params['isAdmin'] ?? null;
		$name = $params['name'] ?? null;

		if (!isset($isAdminRole)) {
			return $this->json(Errors::noRoleIsAdminProvidedError(), 400);
		}

		if (!$name) {
			return $this->json(Errors::noRoleNameProvidedError(), 400);
		}

		// check if already exists
		/** @var  ManagerRegistry $doctrine */
		$doctrine = $this->getDoctrine();

		/** @var RoleRepository $roleRepo */
		$roleRepo = $doctrine->getRepository(Role::class);

		/** @var Role $findDuplicatedRole */
		$findDuplicatedRole = $roleRepo->findOneBy(['name' => $name]);

		if (is_object($findDuplicatedRole)) {
			return $this->json(Errors::duplicatedRoleError(), 400);
		}

		$role = new Role();

		try {
			// try to generate user uuid based on random method
			$role->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		$role->setName($name);
		$role->setIsAdmin($isAdminRole);

		/** @var  ObjectManager $em */
		$em = $doctrine->getManager();

		$em->persist($role);
		$em->flush();

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Role created'
		];

		return $this->json($data);
	}

	/**
	 * Remove a role, only admins can do this action
	 *
	 * @param Request $request request
	 * @param AdminChecker $adminChecker admin checker service
	 * @param string $uuidToDelete uuid
	 *
	 * @return JsonResponse response
	 */
	public function remove(Request $request, AdminChecker $adminChecker, $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check if is an admin
		$isAdminResult = $adminChecker->isAdmin($token);

		// if is an array then an error happened
		if (is_array($isAdminResult)) {
			return $this->json($isAdminResult, 403);
		}

		// get user from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var RoleRepository $roleRepo */
		$roleRepo = $em->getRepository(Role::class);

		/** @var Role $roleToDelete */
		$roleToDelete = $roleRepo->findOneBy([
			'uuid' => $uuidToDelete
		]);

		// if this role isn't found
		if (!is_object($roleToDelete)) {
			return $this->json(Errors::roleNotFoundError(), 404);
		}

		// try to remove
		try {
			$em->remove($roleToDelete);
			$em->flush();
		} catch (Exception $e) {
			return $this->json(Errors::generalDBError());
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Role removed'
		];

		return $this->json($data);
	}

	/**
	 * Edit a role, only admins can do this action
	 *
	 * @param Request $request request
	 * @param AdminChecker $adminChecker admin checker service
	 * @param string $uuidToChange uuid
	 *
	 * @return JsonResponse response
	 */
	public function edit(Request $request, AdminChecker $adminChecker, $uuidToChange) {
		// get token
		$token = $request->headers->get('Auth');

		// check if is an admin
		$isAdminResult = $adminChecker->isAdmin($token);

		// if is an array then an error happened
		if (is_array($isAdminResult)) {
			$code = $isAdminResult['code'];
			return $this->json($isAdminResult, $code);
		}

		//get json
		$json = $request->get('json', null);

		// validate uuid
		$uuidToChange = trim($uuidToChange);
		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}


		// get user from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var RoleRepository $roleRepo */
		$roleRepo = $em->getRepository(Role::class);

		/** @var Role $roleToChange */
		$roleToChange = $roleRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// if this role isn't found
		if (!is_object($roleToChange)) {
			return $this->json(Errors::roleNotFoundError(), 404);
		}

		// get and validate params
		$params = json_decode($json, true);
		$name = $params['name'] ?? null;
		$isAdmin = $params['isAdmin'] ?? null;

		// if nothing is provided then send error
		if (!$name && !isset($isAdmin)) {
			return $this->json(Errors::noRoleDataToChangeError(), 400);
		}

		// set params
		$someChange = false;

		if (trim($name) != $roleToChange->getName()) {
			$someChange = true;
			$roleToChange->setName($name);
		}

		if ($isAdmin != $roleToChange->getIsAdmin()) {
			$someChange = true;
			$roleToChange->setIsAdmin($isAdmin);
		}

		// if don't have some changes then send en error
		if (!$someChange) {
			return $this->json(Errors::nothingToPersistError(), 400);
		}

		// try to persist the role
		try {

			$em->persist($roleToChange);
			$em->flush();

		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'Role edited'
		];

		return $this->json($data);
	}

	/**
	 * List all roles, a user registered in the system can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return Response response
	 */
	public function list(Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$roleRepo = $this->getDoctrine()->getRepository(Role::class);
		$roles = $roleRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($roles,$serializer);
		return $response;
	}

	/**
	 * Show a role, a user registered in the system can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow
	 *
	 * @return JsonResponse response
	 */
	public function show(Request $request, JwtAuth $jwtAuth, $uuidToShow) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get user from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var RoleRepository $roleRepo */
		$roleRepo = $em->getRepository(Role::class);

		/** @var Role $roleToShow */
		$roleToShow = $roleRepo->findOneBy([
			'uuid' => $uuidToShow
		]);

		// if this role isn't found
		if (!is_object($roleToShow)) {
			return $this->json(Errors::roleNotFoundError(), 404);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => $roleToShow
		];

		return $this->json($data);
	}
}
