<?php

namespace App\Controller;

use App\Entity\Column;
use App\Entity\Task;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\ColumnRepository;
use App\Service\JwtAuth;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class ColumnController extends AbstractController {

	/** @var JsonTools */
	private $jsonTools;

	public function __construct() {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Create a column, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse response
	 */
	public function create(Request $request, JwtAuth $jwtAuth) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$title = $params['title'] ?? null;
		$position = $params['position'] ?? null;
		$color = $params['color'] ?? null;

		// validate params
		if (!$title) {
			return $this->json(Errors::noColumnTitleProvidedError(), 400);
		}

		if (!$position) {
			return $this->json(Errors::noColumnPositionProvidedError(), 400);
		}

		// no more columns must be in the same position
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		// set params
		$column = new Column();

		try {
			// try to generate column uuid based on random method
			$column->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		$column->setTitle($title);
		$column->setPosition($position);
		$column->setColor($color);

		// try to persist
		try {
			$em->persist($column);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Column created'
		];

		return $this->json($data);
	}

	/**
	 * Shows a column, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow uuid to show
	 *
	 * @return JsonResponse response
	 */
	public function show(Request $request, JwtAuth $jwtAuth, $uuidToShow) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get column from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var ColumnRepository $columnRepo */
		$columnRepo = $em->getRepository(Column::class);

		/** @var Column $columnToShow */
		$columnToShow = $columnRepo->findOneBy([
			'uuid' => $uuidToShow
		]);

		// if this column isn't found
		if (!is_object($columnToShow)) {
			return $this->json(Errors::columnNotFoundError(), 404);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => $columnToShow
		];

		return $this->json($data);
	}

	/**
	 * Show all columns, somebody registered can do this action
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse|Response response
	 */
	public function list(Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$columnRepo = $this->getDoctrine()->getRepository(Column::class);
		$column = $columnRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($column,$serializer);
		return $response;
	}

	/**
	 * Edit a column, somebody registered can do this action
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToChange uuid to change
	 *
	 * @return JsonResponse response
	 */
	public function edit(Request $request, JwtAuth $jwtAuth, $uuidToChange) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// check uuid
		$uuidToChange = trim($uuidToChange);
		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get column from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var ColumnRepository $columnRepo */
		$columnRepo = $em->getRepository(Column::class);

		/** @var Column $columnToChange */
		$columnToChange = $columnRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// if this column isn't found
		if (!is_object($columnToChange)) {
			return $this->json(Errors::columnNotFoundError(), 404);
		}

		// get params
		$params = json_decode($json, true);
		$title = $params["title"] ?? null;
		$color = $params["color"] ?? null;
		$position = $params['position'] ?? null;

		// set params
		$someChange = false;

		if ($title && trim($title) != $columnToChange->getTitle()) {
			$someChange = true;
			$columnToChange->setTitle($title);
		}

		if ($position && $position != $columnToChange->getPosition()) {
			$someChange = true;
			$columnToChange->setPosition($position);
		}

		if ($color && trim($color) != $columnToChange->getColor()) {
			$someChange = true;
			$columnToChange->setColor($color);
		}

		// if don't have some changes then send en error
		if (!$someChange) {
			return $this->json(Errors::nothingToPersistError(), 400);
		}

		// try to persist the column
		try {

			$em->persist($columnToChange);
			$em->flush();

		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'Column edited'
		];

		return $this->json($data);
	}

	/**
	 * Remove a column, somebody registered can do this action
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToDelete uuid to delete
	 *
	 * @return JsonResponse response
	 */
	public function remove(Request $request, JwtAuth $jwtAuth, $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get column from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var ColumnRepository $columnRepo */
		$columnRepo = $em->getRepository(Column::class);

		/** @var Column $columnToDelete */
		$columnToDelete = $columnRepo->findOneBy([
			'uuid' => $uuidToDelete
		]);

		// if this column isn't found
		if (!is_object($columnToDelete)) {
			return $this->json(Errors::columnNotFoundError(), 404);
		}

		// search tasks in column
		$taskRepo = $em->getRepository(Task::class);
		$tasksInColumn =
			$taskRepo->findBy(['_column' => $columnToDelete->getId()]);

		// if there are some task in the column then error
		if ($tasksInColumn && count($tasksInColumn) > 0) {
			return $this->json(Errors::tasksInColumnError(), 403);
		}

		// try to remove
		try {
			$em->remove($columnToDelete);
			$em->flush();
		} catch (ORMException $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Column removed'
		];

		return $this->json($data);
	}
}
