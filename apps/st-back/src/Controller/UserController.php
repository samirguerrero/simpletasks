<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Helper\Errors;
use App\Helper\JsonTools;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Service\JwtAuth;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 *
 * Class UserController
 *
 * @package App\Controller
 */
class UserController extends AbstractController {

	/** @var JsonTools */
	private $jsonTools;

	function __construct () {
		$this->jsonTools = new JsonTools();
	}

	/**
	 * Login method, need a mail and password and return
	 * a token, a token decoded with user data or an error
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service by DI
	 *
	 * @return JsonResponse
	 */
	public function login (Request $request, JwtAuth $jwtAuth) {
		// get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get params
		$params = json_decode($json, true);
		$mail = $params['mail'] ?? null;
		$plainPassword = $params['password'] ?? null;
		$tokenDecoded = $params['getTokenDecoded'] ?? null;

		// param validations
		$validateEmail = $this->validateEmail($mail);
		if (count($validateEmail) > 0) {
			return $this->json(Errors::userMailWrongError(), 400);
		}

		if (!$plainPassword) {
			return $this->json(Errors::userPasswordEmptyError(), 400);
		}

		// set user params
		$user = new User();
		$user->setMail($mail);
		$user->setPassword($plainPassword);

		$loginResult =
			$jwtAuth->signUp($mail,$user->getPassword(),$tokenDecoded);

		// if there is a login error jwtAuth return false
		if (!$loginResult) {
			return $this->json(Errors::loginError(), 404);
		}

		// return a token or a user
		$result = $tokenDecoded ? json_encode($loginResult) : $loginResult;

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'User login correct',
			'result' =>  $result
		];

		return $this->json($data);
	}

	/**
	 * Register a new user, if any error occurs then return an exception
	 * and don't create a new user
	 *
	 * @param Request $request
	 *
	 * @return Response json response, error o success
	 */
	public function register (Request $request) {

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get params
		$params = json_decode($json, true);
		$name = $params['name'] ?? null;
		$mail = $params['mail'] ?? null;
		$avatar = $params['avatar'] ?? null;
		$plainPassword = $params['password'] ?? null;

		// initial params validations

		if (!$name) {
			return $this->json(Errors::userNameEmptyError(), 400);
		}

		$validateEmail = $this->validateEmail($mail);
		if (!$mail || count($validateEmail) > 0) {
			return $this->json(Errors::userMailWrongError(), 400);
		}

		if (!$plainPassword) {
			return $this->json(Errors::userPasswordEmptyError(), 400);
		}

		// create a new User and set its properties
		$user = new User();

		/** @var ManagerRegistry $doctrine */
		$doctrine = $this->getDoctrine();

		/** @var UserRepository $userRepo */
		$userRepo = $doctrine->getRepository(User::class);

		$exists = $this->checkDuplicatedEmail($userRepo, $mail);

		if ($exists == 1) {
			// if exists then return duplicated error
			return $this->json(Errors::userDuplicatedError(), 400);
		}

		$user->setMail($mail);

		try {
			// try to generate user uuid based on random method
			$user->setUuid(Uuid::uuid4());
		} catch (Exception $e) {
			// an exception occurs inner uuid library
			return $this->json(Errors::uuidError(), 500);
		}

		// to set the role first we need to check if is the first user
		// to register in this app, if it is then is an admin user, if not is
		// a normal user
		$totalUsers = count(
			$userRepo->findAll()
		);

		/** @var Role $role */
		$role = null;

		/** @var RoleRepository $roleRepo */
		$roleRepo = $doctrine->getRepository(Role::class);

		$status = 0;

		// retrieve admin role to set it
		if ($totalUsers == 0) {
			$role = $roleRepo->find(1);

			// if no exists then create admin role
			if (!is_object($role)) {

				$role = new Role();
				$roleUuid = null;

				try {
					$roleUuid = Uuid::uuid4();
				} catch (Exception $e) {
					return $this->json(Errors::uuidError(), 500);
				}

				$role->setUuid($roleUuid);
				$role->setIsAdmin(1);
				$role->setName("Admin");

				$doctrine->getManager()->persist($role);
				$doctrine->getManager()->flush();
			}

			// if is an admin then is activated by default
			$status = 1;
		}
		else {
			// retrieve default role (user) to set it
			$role = $roleRepo->find(2);

			// if no exists then create logged role
			if (!is_object($role)) {

				$role = new Role();
				$roleUuid = null;

				try {
					$roleUuid = Uuid::uuid4();
				} catch (Exception $e) {
					return $this->json(Errors::uuidError(), 500);
				}

				$role->setUuid($roleUuid);
				$role->setIsAdmin(0);
				$role->setName("Logged");

				$doctrine->getManager()->persist($role);
				$doctrine->getManager()->flush();
			}
		}

		$user->setRole($role);

		// set common properties
		$user->setPassword($plainPassword);
		$user->setName($name);
		$user->setAvatar($avatar);
		$user->setStatus($status); //0 default, 1 admin default

		/** @var ObjectManager $objectManager */
		$objectManager = $doctrine->getManager();

		$objectManager->persist($user);
		$objectManager->flush();

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'User registered'
		];

		return $this->json($data);
	}

	/**
	 * Return all users
	 *
	 * @param Request $request
	 * @param JwtAuth $jwtAuth
	 *
	 * @return Response
	 */
	public function list (Request $request, JwtAuth $jwtAuth) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(),400);
		}

		// only admins can list users, therefore we have to check if is an admin
		// before list them

		// get identity
		$identity = $jwtAuth->getIdentity($token);

		// check user in db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		/** @var User $userLogged */
		$userLogged = $userRepo->findOneBy(['uuid' => $identity->sub]);

		// if user isn't an admin then return an error
		if ($userLogged->getRole()->getIsAdmin() == 0) {
			return $this->json(Errors::insufficientPermissionsError(),403);
		}

		// is an admin user, get the user list

		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');
		$userRepo = $this->getDoctrine()->getRepository(User::class);
		$users = $userRepo->findAll();

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($users,$serializer);
		return $response;
	}

	/**
	 * Retrieve a single user data if is the same user or if is an admin user,
	 * if a user that isn't a admin user request other user data an error occurs
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToShow user uuid to show
	 *
	 * @return JsonResponse response
	 */
	public function show (Request $request, JwtAuth $jwtAuth, string $uuidToShow) {

		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// validate if the uuid is present
		$uuidToShow = trim($uuidToShow);

		if (!$uuidToShow) {
			return $this->json(Errors::uuidError(), 500);
		}

		// only admin users can display other users data, therefore must be check
		// if the request user is the same user to show

		// get user data from the token
		$identity = $jwtAuth->getIdentity($token);

		// get user from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		/** @var User $userLogged */
		$userLogged = $userRepo->findOneBy([
			'uuid' => $identity->sub
		]);

		// check if is an admin or is the same user
		if ($userLogged->getUuid() != trim($uuidToShow) &&
		    $userLogged->getRole()->getIsAdmin() == 0) {
			return $this->json(Errors::insufficientPermissionsError(), 403);
		}

		// get user from db and send it
		$userToShow = $userRepo->findOneBy(['uuid' => $uuidToShow]);
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'result' => $userToShow
		];

		return $this->json($data);
	}

	/**
	 * Edit user data if is the same user logged or is an admin
	 *
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 *
	 * @return JsonResponse response
	 */
	public function edit (Request $request, JwtAuth $jwtAuth) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		// get params from request
		$json = $request->get('json', null);

		// if the token isn't
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get params
		$params = json_decode($json, true);
		$uuidToChange = $params['uuid'] ?? null;

		if (!$uuidToChange) {
			return $this->json(Errors::uuidError(), 500);
		}

		$name = $params['name'] ?? null;
		$mail = $params['mail'] ?? null;
		$avatar = $params['avatar'] ?? null;
		$plainPassword = $params['password'] ?? null;
		$role = $params['role'] ?? null;
		$status = $params['status'] ?? 0;

		// if nothing is provided then send error
		if (!$name && !$mail && !$avatar && !$plainPassword && !$role && !$status) {
			return $this->json(Errors::noUserDataToChangeError(), 404);
		}

		// get user data from the token
		$identity = $jwtAuth->getIdentity($token);

		// get user from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		/** @var User $userLogged */
		$userLogged = $userRepo->findOneBy([
			'uuid' => $identity->sub
		]);
		/** @var User $userToChange */
		$userToChange = $userRepo->findOneBy([
			'uuid' => $uuidToChange
		]);

		// check if is an admin or is the same user
		if ($userLogged->getUuid() != trim($uuidToChange) &&
		    $userLogged->getRole()->getIsAdmin() == 0) {
			return $this->json(Errors::insufficientPermissionsError(), 403);
		}

		// set user data only when is provided
		$someChanged = false;

		// name
		if ($name && $userToChange->getName() != trim($name)) {
			$userToChange->setName(trim($name));
			$someChanged = true;
		}

		// mail
		if ($mail) {
			// email validation
			$validEmail = count($this->validateEmail($mail));

			// validation error will send it
			if ($validEmail > 0) {
				return $this->json(Errors::userMailWrongError(), 400);
			}

			// only set it when an email provided is different than the actual one and
			// anybody more haven't this email account
			if ($mail != $userToChange->getMail() &&
			    !$this->checkDuplicatedEmail($userRepo, $mail)) {
				$userToChange->setMail($mail);
				$someChanged = true;
			}
		}

		// avatar
		if ($avatar && $userToChange->getAvatar() != trim($avatar)) {
			$userToChange->setAvatar($avatar);
			$someChanged = true;
		}

		// password
		$encryptedPassword = hash('md5', $plainPassword);
		if ($plainPassword && $encryptedPassword != $userToChange->getPassword()) {
			$userToChange->setPassword($plainPassword);
			$someChanged = true;
		}

		// status
		if ($status != $userToChange->getStatus()) {
			$userToChange->setStatus($status);
			$someChanged = true;
		}

		// role
		if ($role) {

			// get new role
			/** @var RoleRepository $roleRepo */
			$roleRepo = $em->getRepository(Role::class);
			/** @var Role $roleReg */
			$roleReg = $roleRepo->findOneBy([
				'uuid' => $role
			]);

			// only set it when is a different role
			if ($roleReg->getUuid() != $userToChange->getRole()->getUuid()) {
				$userToChange->setRole($roleReg);
				$someChanged = true;
			}
		}

		//  if there are some change try to persist
		if ($someChanged) {

			try {

				$em->persist($userToChange);
				$em->flush();

			} catch (ORMException $e) {
				return $this->json(Errors::generalDBError());
			}

			// return response ok
			$data = [
				'status'  => 'success',
				'code'    => 200,
				'message' => 'User edited'
			];
			return $this->json($data);
		}

		// if there aren't something to persist send an error
		return $this->json(Errors::nothingToPersistError(), 400);
	}

	/**
	 * Remove a user, only admins other users or the same user logged can be remove
	 * its self
	 * @param Request $request request
	 * @param JwtAuth $jwtAuth jwt service
	 * @param string $uuidToDelete
	 *
	 * @return JsonResponse
	 */
	public function remove (Request $request, JwtAuth $jwtAuth, string $uuidToDelete) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}

		$uuidToDelete = trim($uuidToDelete);

		// validate if the uuid is present
		if (!$uuidToDelete) {
			return $this->json(Errors::uuidError(), 500);
		}

		// only admin users can delete other users, therefore must be check
		// if the request user is the same user to delete

		// get user data from the token
		$identity = $jwtAuth->getIdentity($token);

		// get user from db
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		/** @var User $userLogged */
		$userLogged = $userRepo->findOneBy([
			'uuid' => $identity->sub
		]);

		// check if is an admin or is the same user
		if ($userLogged->getUuid() != trim($uuidToDelete) &&
		    $userLogged->getRole()->getIsAdmin() == 0) {
			return $this->json(Errors::insufficientPermissionsError(), 403);
		}

		try {
			/** @var User $userToRemove */
			$userToRemove = $userRepo->findOneBy(['uuid' => $uuidToDelete]);
			$em->remove($userToRemove);
			$em->flush();
		} catch (Exception $e) {
			return $this->json(Errors::generalDBError(), 500);
		}

		// return response ok
		$data = [
			'status'  => 'success',
			'code'    => 200,
			'message' => 'User removed'
		];
		return $this->json($data);
	}

	/**
	 * Search users given a name
	 *
	 * @param Request $request
	 * @param JwtAuth $jwtAuth
	 * @param string $name
	 *
	 * @return Response
	 */
	public function searchByName(Request $request, JwtAuth $jwtAuth, string $name) {
		// get token
		$token = $request->headers->get('Auth');

		// check token validity
		if (!$jwtAuth->checkValidToken($token)) {
			return $this->json(Errors::invalidTokenError(), 400);
		}


		// create a query to search by name and execute it
		/** @var EntityManager $em */
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		$query = $userRepo->createQueryBuilder('u');
		$query->addSelect(
				"
		 (CASE WHEN u.name like '". $name ."' THEN 0
           WHEN u.name like '". $name ."%' THEN 1
           WHEN u.name like '%". $name ."' THEN 2
           WHEN u.name like '%". $name ."%' THEN 3
           ELSE 4 
      END) 
      AS HIDDEN ORD ");
		$query->orderBy('ORD', 'ASC');

		$users = $query->getQuery()->getResult();

		// serialize the result to send it in the response
		/** @var Serializer $serializer */
		$serializer = $this->get('serializer');

		/** @var Response $response */
		$response = $this->jsonTools->dataToJson($users,$serializer);
		return $response;
	}

	/**
	 * Validation $email with Validator
	 * @param $mail user mail
	 *
	 * @return ConstraintViolationListInterface
	 */
	private function validateEmail($mail) {
		/** @var ValidatorInterface $validator */
		$validator = Validation::createValidator();

		return $validator->validate($mail,[
			new Email()
		]);
	}

	/**
	 * Check if an email provided exists in the system
	 *
	 * @param UserRepository $userRepo userRepo
	 * @param string $mail mail
	 *
	 * @return boolean true when exists
	 */
	private function checkDuplicatedEmail(UserRepository $userRepo, $mail) {
		// check duplicated
		return count(
			$userRepo->findBy([
				'mail' => $mail
			])
		) > 0;
	}
}
