<?php

namespace App\Controller;

use App\Entity\User;
use App\Helper\Errors;
use App\Helper\MailMessages;
use App\Repository\UserRepository;
use App\Service\MailSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class MailController extends AbstractController{

  public function resetPassword(Request $request, MailSender $mailSender, UserRepository $userRepository) {
	  //get json
	  $json = $request->get('json', null);

	  // initial validation
	  if (!$json) {
		  return $this->json(Errors::jsonEmptyError(), 400);
	  }

	  // get and validate params
	  $params = json_decode($json, true);
	  $mail = $params['mail'] ?? null;

		if (!$mail) {
			return $this->json(Errors::userMailWrongError(), 400);
		}

		$user = $userRepository->findOneBy(['mail' => $mail]);

		if (!is_object($user)) {
			return $this->json(Errors::userNotFoundError(), 404);
		}

	  $encryptedMail =
		  openssl_encrypt($mail, $_ENV['CIPHER'], $_ENV['SALT'], 0, $_ENV['IV']);

		$url =
			$_ENV['APP_FRONT_BASE_PATH'] .
			'/reset-password-finish/' . $encryptedMail;

		$result = $mailSender->send(
			MailMessages::subjectSendResetMail(),
			$mail,
			MailMessages::bodySendResetMail() . ' ' . $url
		);

		if (!$result) {
			return $this->json(Errors::mailError(), 500);
		}

	  $data = [
		  'status' => 'success',
		  'code' => 200,
		  'message' => 'Email send it to user with reset link'
	  ];

	  return $this->json($data);
  }

	public function resetPasswordFinishAction(Request $request, UserRepository $userRepository, string $hash) {

  	// security checks
  	if (!$hash) {
		  return $this->json(Errors::operationNotAllowed(), 403);
	  }

		$mail =
			openssl_decrypt(
				$hash,
				$_ENV['CIPHER'],
				$_ENV['SALT'],
				0,
				$_ENV['IV']);

  	// security check
  	if (!$mail) {
		  return $this->json(Errors::operationNotAllowed(), 403);
	  }

		//get json
		$json = $request->get('json', null);

		// initial validation
		if (!$json) {
			return $this->json(Errors::jsonEmptyError(), 400);
		}

		// get and validate params
		$params = json_decode($json, true);
		$pass = $params['pass'];

		if (!$pass) {
			return $this->json(Errors::userPasswordEmptyError(), 400);
		}

		/** @var User $user */
		$user = $userRepository->findOneBy(['mail' => $mail]);

		if (!is_object($user)) {
			return $this->json(Errors::userNotFoundError(), 404);
		}

		// set new data and persists
		$user->setPassword($pass);
		$this->getDoctrine()->getManager()->persist($user);

		$data = [
			'status' => 'success',
			'code' => 200,
			'message' => 'Password changed'
		];

		return $this->json($data);
	}
}
