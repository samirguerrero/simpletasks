<?php

namespace App\Helper;

class Errors {

	// GENERAL //////////////////////////////////////////////////////////
	public static function jsonEmptyError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Empty json in request'
		];
	}

	public static function fileEmptyError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Empty file in request'
		];
	}

	public static function fileError() {
		return [
			'status' => 'error',
			'code' => 500,
			'message' => "Can't upload the file"
		];
	}

	public static function mailError() {
		return [
			'status' => 'error',
			'code' => 500,
			'message' => 'Some error when sent an email'
		];
	}

	public static function notAllowedOrigin() {
		return [
			'status' => 'error',
			'code' => 403,
			'message' => 'Not allowed origin request'
		];
	}

	public static function operationNotAllowed() {
		return [
			'status' => 'error',
			'code' => 403,
			'message' => 'Operation not allowed'
		];
	}

	public static function notAllowedMimeType() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Not allowed to use this mime type'
		];
	}

	public static function invalidTokenError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Invalid token'
		];
	}

	public static function insufficientPermissionsError() {
		return [
			'status' => 'error',
			'code' => 403,
			'message' => 'Insufficient Permissions to do this action'
		];
	}

	public static function dateFormatError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Date format error'
		];
	}

	// DATABASE  ////////////////////////////////////////////////////////
	public static function uuidError() {
		return [
			'status' => 'error',
			'code' => 500,
			'message' => 'Some error related with uuid'
		];
	}

	public static function generalDBError() {
		return [
			'status' => 'error',
			'code' => 500,
			'message' => 'Some error related with db'
		];
	}

	public static function nothingToPersistError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No changes to persist'
		];
	}

	// USER /////////////////////////////////////////////////////////////
	public static function userNameEmptyError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Empty user name in request'
		];
	}

	public static function userMailWrongError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Wrong or empty mail in request'
		];
	}

	public static function userPasswordEmptyError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Empty password in request'
		];
	}

	public static function userDuplicatedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'This mail has been taken'
		];
	}

	public static function loginError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'User or Password are incorrect or maybe your user is deactivated'
		];
	}

	public static function noUserDataToChangeError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'No user data is given'
		];
	}

	public static function userNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'User not found'
		];
	}

	// ROLE /////////////////////////////////////////////////////////////
	public static function noRoleNameProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No role name is given'
		];
	}

	public static function noRoleIsAdminProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No role admin param is given'
		];
	}

	public static function duplicatedRoleError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Role name taken'
		];
	}

	public static function roleNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'Role not found'
		];
	}

	public static function noRoleDataToChangeError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No role data is given'
		];
	}

	// COLUMN /////////////////////////////////////////////////////////
	public static function noColumnTitleProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No column title provided'
		];
	}

	public static function noColumnPositionProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No column position provided'
		];
	}

	public static function columnPositionError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'Column position taken'
		];
	}

	public static function columnNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'Column not found'
		];
	}

	public static function tasksInColumnError() {
		return [
			'status' => 'error',
			'code' => 403,
			'message' => 'Tasks in columns, try to remove all tasks'
		];
	}

	// PROJECT /////////////////////////////////////////////////////////
	public static function noProjectTitleProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No project title provided'
		];
	}

	public static function projectNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'Project not found'
		];
	}

	// TAG /////////////////////////////////////////////////////////////
	public static function noTagTitleProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No tag title provided'
		];
	}

	public static function tagNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'Tag not found'
		];
	}

	// TASK /////////////////////////////////////////////////////////////
	public static function noTaskTitleProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No task title provided'
		];
	}

	public static function taskNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'Task not found'
		];
	}

	// TASK_TAG /////////////////////////////////////////////////////////
	public static function noTaskIdeProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No task_id provided'
		];
	}

	public static function noTagIdProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No tag_id provided'
		];
	}

	public static function taskTagNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'TaskTag not found'
		];
	}

	// MENU_OPTION ///////////////////////////////////////////////////////
	public static function noMenuOptionTitleProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No menu title provided'
		];
	}

	public static function noMenuIconTitleProvidedError() {
		return [
			'status' => 'error',
			'code' => 400,
			'message' => 'No menu icon provided'
		];
	}

	public static function menuOptionNotFoundError() {
		return [
			'status' => 'error',
			'code' => 404,
			'message' => 'MenuOption not found'
		];
	}
}
