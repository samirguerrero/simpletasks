<?php

namespace App\Helper;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class JsonTools {

	/**
	 * @param $data mixed Object to serialize
	 *
	 * @param Serializer $serializer serializer
	 *
	 * @return Response response with status code 200
	 *
	 */
	public function dataToJson ($data, Serializer $serializer) {

		$json = $serializer->serialize($data, 'json');

		$response = new Response();

		$response->headers->set('Content-Type', 'application/json');

		$data = [
			'status' => 'success',
			'code' => 200,
			'result' => json_decode($json)
		];

		$response->setContent(json_encode($data));
		$response->setStatusCode(200, 'success');

		return $response;
	}
}
