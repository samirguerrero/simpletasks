<?php

namespace App\Helper;

class MailMessages {

	public static function subjectSendResetMail() {
		return 'Reset Password from SimpleTasks';
	}

	public static function bodySendResetMail() {
		return 'Someone has made a request to change your account password in 
		SimpleTasks, if you made it you can continue with the process 
		by clicking on the following link, if you did not ignore this email.';
	}
}
