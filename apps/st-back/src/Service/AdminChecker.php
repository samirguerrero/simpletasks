<?php

namespace App\Service;

use App\Entity\User;
use App\Helper\Errors;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;

class AdminChecker {

	/** @var EntityManager */
	public $em;

	/** @var JwtAuth */
	public $jwtAuth;

	public function __construct(EntityManager $entityManager, JwtAuth $jwtAuth) {
		$this->em = $entityManager;
		$this->jwtAuth = $jwtAuth;
	}

	/**
	 * @param $token
	 *
	 * @return array|bool
	 */
	public function isAdmin($token) {

		// check token validity
		if (!$this->jwtAuth->checkValidToken($token)) {
			return Errors::invalidTokenError();
		}

		// only admins can list users, therefore we have to check if is an admin
		// before list them

		// get identity
		$identity = $this->jwtAuth->getIdentity($token);

		// check user in db

		/** @var UserRepository $userRepo */
		$userRepo = $this->em->getRepository(User::class);

		/** @var User $userLoged */
		$userLoged = $userRepo->findOneBy(['uuid' => $identity->sub]);

		// if user isn't an admin then return an /** @var EntityManager $em */
		if ($userLoged->getRole()->getIsAdmin() == 0) {
			return Errors::insufficientPermissionsError();
		}

		return true;
	}
}
