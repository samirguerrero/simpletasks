<?php

namespace App\DataFixtures;

use App\Entity\MenuOption;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture {

	public function __construct() {}

	/**
	 * @param ObjectManager $manager
	 *
	 * @throws \Exception
	 */
	public function load(ObjectManager $manager) {
    $menuOptionsData = $this->getMenuItems();

    foreach ($menuOptionsData as $menuOptionData) {
    	$menuOption = new MenuOption();
    	$menuOption->setUuid($menuOptionData['uuid']);
    	$menuOption->setTitle($menuOptionData['title']);
    	$menuOption->setIcon($menuOptionData['icon']);
	    $manager->persist($menuOption);
    }

    $projectData = $this->getFirstProject();

    $project = new Project();
    $project->setTitle($projectData['title']);
    $project->setUuid($projectData['uuid']);
    $manager->persist($project);

    $manager->flush();
	}

	/**
	 * Generate menu items
	 *
	 * @return array|array[]
	 */
	private function getMenuItems(): array {
	  return [
	    [
				'id' => '1',
		    'uuid' => '89bf61b3-e21f-4136-87ac-b22b0f013c4e',
		    'title' => 'Profile',
		    'icon' => 'perm_identity'
	    ],
		  [
			  'id' => '2',
			  'uuid' => 'b0920bd3-efac-4a75-bfef-e10ffe42fd66',
			  'title' => 'Logout',
			  'icon' => 'exit_to_app'
		  ],
		  [
			  'id' => '3',
			  'uuid' => 'f4506607-7aeb-402b-a224-3d8d863576c1',
			  'title' => 'Users admin',
			  'icon' => 'vpn_key'
		  ],
		  [
			  'id' => '4',
			  'uuid' => 'ab7b9e77-7f33-440c-a228-db6e9025cc19',
			  'title' => 'Add column',
			  'icon' => 'playlist_add'
		  ],
		  [
			  'id' => '5',
			  'uuid' => 'd9da014c-46fe-4f44-ac54-c6bac8caeba5',
			  'title' => 'Add task',
			  'icon' => 'exposure_plus_1'
		  ],
		  [
			  'id' => '6',
			  'uuid' => '721c8729-ebe0-4d3b-a8ee-8c88ebe298f7',
			  'title' => 'Tasks',
			  'icon' => 'dashboard'
		  ],
	  ];
	}

	/**
	 * Generate the first dummy project
	 *
	 * @return array|string[]
	 */
	private function getFirstProject(): array {
		return [
			'uuid' => '721c8729-ebe0-4d3b-a8ee-8c88ebe298f8',
			'title' => 'Dummy project'
		];
	}

}
