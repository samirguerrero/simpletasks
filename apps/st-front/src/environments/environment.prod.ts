export const environment = {
  production: true,
  backBaseApiPath: 'https://simple-tasks-api.samsoft.es/api/v1',
  backBasePath: 'https://simple-tasks-api.samsoft.es'
};
