export class Strings {
  public static NEW_TASK_PAGE_TITLE: string = "New task";
  public static UPDATE_TASK_PAGE_TITLE: string = "Update task";
  public static TASK_CREATED_MESSAGE: string = "Task created, go to task list";
  public static TASK_UPDATED_MESSAGE: string = "Task updated, go to task list";
  public static TASK_DELETE_DIALOG_MESSAGE: string = "Are you sure to remove this task?";
  public static ADD_COLUMN_OPTION_MESSAGE: string = "Add column";
  public static ADD_TASK_OPTION_MESSAGE: string = "Add task";
  public static LOGOUT_OPTION_MESSAGE: string = "Logout";
  public static FORGOT_MAIL_MESSAGE: string = "A mail is travelling around the wires and waves, check your mail account to reset your password";
  public static ALREADY_LOGGED_MESSAGE: string = "Welcome to Simple Tasks. You are already logged";
  public static REGISTERED_SUCCESS_MESSAGE: string = "Register completed but your user must be activated by an Admin before you can use this apps";
  public static ROLE_CHANGED_SUCCESS_MESSAGE: string = "Role changed";
  public static STATUS_CHANGED_SUCCESS_MESSAGE: string = "Status changed";
  public static USER_DELETE_WARNING_MESSAGE: string = "Are your sure to delete the user: ";
}
