import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-three-dots-menu',
  templateUrl: './three-dots-menu.component.html',
  styleUrls: ['./three-dots-menu.component.scss']
})
export class ThreeDotsMenuComponent implements OnInit {

  // title options array
  @Input('options') options: string[];

  // option click propagate emitter
  @Output() clickOptionEmmit: EventEmitter<number>;

  // view elements to manipulate
  @ViewChild('middle',null) middle: ElementRef;
  @ViewChild('cross',null) cross: ElementRef;
  @ViewChild('dropdown',null) dropdown: ElementRef;

  constructor() {
    // init data
    this.options = [];
    this.clickOptionEmmit = new EventEmitter<number>();
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void { }

  /**
   * Called from the ui, click menu event
   */
  clickMenu(): void {
    // toggle classes to show or hide the options dropdown
    this.middle.nativeElement.classList.toggle('active');
    this.cross.nativeElement.classList.toggle('active');
    this.dropdown.nativeElement.classList.toggle('active');
  }

  /**
   * Called from the ui, click option event
   *
   * @param index clicked option
   */
  clickOption(index: number): void {
    this.clickOptionEmmit.emit(index);
  }
}
