import {Component, EventEmitter, Output} from '@angular/core';
import {Router} from '@angular/router';
import {Cons} from '../../helpers/Cons';
import {UserService} from '../../services/user.service';
import {Strings} from '../../helpers/Strings';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  // sidebar internal menu flag
  isShowed: boolean ;

  // menu options
  threeDotsMenu: string[];

  // sidebar show flag
  @Output() isSideMenuShowedEvEmit;

  // add column option selected event
  @Output() addColumnEmmit;

  constructor(
    private router: Router,
    private userService: UserService) {
    // init data
    this.isShowed = false;
    this.isSideMenuShowedEvEmit = new EventEmitter<boolean>();
    this.addColumnEmmit = new EventEmitter<any>();
    this.threeDotsMenu = [
      Strings.ADD_COLUMN_OPTION_MESSAGE,
      Strings.ADD_TASK_OPTION_MESSAGE,
      Strings.LOGOUT_OPTION_MESSAGE
    ];
  }

  /**
   * Hamburger menu click event,
   * called from the ui
   */
  showSideMenuOnClick(): void {
    this.isShowed = !this.isShowed;
    this.isSideMenuShowedEvEmit.emit(this.isShowed);
  }

  /**
   * Profile button click event,
   * call it from the ui
   */
  clickProfile(): void {
    this.router.navigate([Cons.frontProfileListPath]).then(() => {});
  }

  /**
   * Click menu option event
   *
   * @param optionSelected number selected option index
   */
  menuOptionClick(optionSelected: number): void {
    switch (optionSelected) {
      case 0: // add column
        this.addColumnEmmit.emit();
        break;
      case 1: // new task
        this.router.navigate([Cons.frontNewTaskPath]).then(() => {});
        break;
      case 2: // logout
        this.userService.logOut();
        // navigate to login
        location.href = '/';
        break;
    }
  }
}
