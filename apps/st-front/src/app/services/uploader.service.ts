import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Cons} from '../helpers/Cons';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploaderService {

  constructor(private httpClient: HttpClient) { }

  /**
   * Make a call to upload files
   *
   * @param data FormData with the file to upload
   * @param secured if is an registered user need secured header
   * @return post call observable
   */
  public upload(data: FormData, secured: boolean = true): Observable<any> {
    // basic configuration and security header
    const securedPath = secured ? Cons.uploadPathSecured : Cons.uploadPathUnsecured;
    const uploadURL = environment.backBaseApiPath + securedPath;

    const token = localStorage.getItem('token');
    let headers = new HttpHeaders({});

    // only register call is an insecure call, the rest must be secured
    if (secured) {
      headers = new HttpHeaders({
        'Auth': token
      });
    }

    // return post call observable
    return this.httpClient.post(
      uploadURL,
      data, {
        reportProgress: true,
        observe: 'events',
        headers: headers
      }
    );
  }
}
