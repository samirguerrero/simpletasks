import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ParamMakerService} from './param-maker.service';
import {UserService} from './user.service';
import {SimpleTaskService} from './simple-task.service';
import {Observable, throwError} from 'rxjs';
import {Cons} from '../helpers/Cons';
import {Errors} from '../helpers/Errors';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TagsService extends SimpleTaskService{

  constructor(private client: HttpClient,
              private paramMakerService: ParamMakerService,
              private userService: UserService) {
    super(client, paramMakerService, userService);
    this.classType = 'tag';
  }

  /**
   * Search tags in the server given a title
   *
   * @param tagTitle
   * @return Observable<any> get call observable
   */
  searchByTitle(tagTitle: string): Observable<any> {

    // check if the user is logged in
    if (!this.userService.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    // create the call with the term to search
    const url = environment.backBaseApiPath + '/tag' + Cons.findByTitlePath + tagTitle;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api
    return this.client.get(
      url,
      { headers: headers }
    );
  }
}
