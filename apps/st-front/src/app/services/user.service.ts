import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User';
import {Cons} from '../helpers/Cons';
import {ParamMakerService} from './param-maker.service';
import {Observable, throwError} from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import {Errors} from '../helpers/Errors';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private client: HttpClient, private paramMakerService: ParamMakerService) {}

  /**
   * Register a user given.
   *
   * @param user
   */
  register(user: User): Observable<any> {
    // create the json to send with the user and other stuff
    const params = this.paramMakerService.getParams(user);
    const url = environment.backBaseApiPath + Cons.registerPath;

    // call the api to try to register the user given
    return this.client.post(
      url,
      params,
      { headers: Cons.unprivilegedHeader }
    );
  }

  /**
   * Login a user, if is success then return an auth token
   *
   * @param user
   */
  login(user: User): Observable<any> {
    // create the json to send with the user and other stuff
    const params = this.paramMakerService.getParams(user);
    const url = environment.backBaseApiPath + Cons.loginPath;

    // call the api to try to login the user given
    return this.client.post(
      url,
      params,
      { headers: Cons.unprivilegedHeader }
    );
  }

  /**
   * Send an request to the server to send 1an recovery mail
   *
   * @param mail
   */
  forgotSendEmail(mail: string): Observable<any> {
    // create the json to send with the user and other stuff
    const params = this.paramMakerService.getParams({'mail':mail});
    const url = environment.backBaseApiPath + Cons.forgotPassPath;

    // call the api to try to login the user given
    return this.client.post(
      url,
      params,
      { headers: Cons.unprivilegedHeader }
    );
  }

  /**
   * Change user password request
   *
   * @param password
   * @param hash
   */
  forgotChangePass(password: string, hash: string): Observable<any> {
    // create the json to send with the user and other stuff
    const params = this.paramMakerService.getParams({'pass':password});
    const url = environment.backBaseApiPath + Cons.forgotPassPathFinish + hash;

    // call the api to try to login the user given
    return this.client.post(
      url,
      params,
      { headers: Cons.unprivilegedHeader }
    );
  }


  /**
   * Get an user given him uuid
   *
   * @param user
   */
  show(user: User): Observable<any> {

    if (!this.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    // create the json to send with the user and other stuff
    const url = environment.backBaseApiPath + '/user' + Cons.showPath + user.uuid;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api to try to register the user given
    return this.client.get(
      url,
      { headers: headers }
    );
  }

  /**
   * Search users given a name
   *
   * @param userName
   */
  searchByName(userName: string) {

    // check if the user is logged in
    if (!this.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    // create the request the the term to search
    const url = environment.backBaseApiPath + '/user' + Cons.findByNamePath + userName;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api
    return this.client.get(
      url,
      { headers: headers }
    );
  }

  /**
   * Edit an user given its uuid
   *
   * @param user
   */
  edit(user: User): Observable<any> {

    if (!this.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    // create the json to send with the user and other stuff
    const url = environment.backBaseApiPath + '/user/edit';
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);
    const params = this.paramMakerService.getParams(user);

    // call the api to try to register the user given
    return this.client.put(
      url,
      params,
      { headers: headers }
    );
  }

  /**
   * Remove an user given its uuid
   *
   * @param user
   */
  remove(user: User): Observable<any> {

    if (!this.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    // create the json to send with the user and other stuff
    const url = environment.backBaseApiPath + '/user' + Cons.removePath + user.uuid;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);

    // call the api to try to register the user given
    return this.client.delete(
      url,
      { headers: headers }
    );
  }

  /**
   * Get a list of users
   */
  list(): Observable<any> {

    if (!this.isLogged()) {
      return throwError(Errors.NOT_LOGGED_ERROR);
    }

    // create the json to send with the user and other stuff
    const url = environment.backBaseApiPath + '/user' + Cons.listPath;
    const token = localStorage.getItem('token');
    const headers = Cons.privilegedHeader(token);


    // call the api to try to register the user given
    return this.client.get(
      url,
      { headers: headers }
    );
  }

  /**
   * Check if token is valid or exists
   */
  public isLogged(): boolean {

    const token = localStorage.getItem('token');

    // not logged
    if (token == null) {
      return false;
    }

    const tokenDecoded = this.getTokenDecoded(token);

    // an error decoding token
    if (!tokenDecoded) {
      return false;
    }

    const expTime = tokenDecoded.exp * 1000;
    const now =  new Date().getTime();

    // check if is expired
    if (now >= expTime) {
      this.logOut();
      return false;
    }

    return true;
  }

  /**
   * Set token in local storage
   *
   * @param response
   */
  public setTokenData(response: any): void {
      localStorage.setItem('token', response.result);
  }

  /**
   * Remove token data from local storage
   */
  public logOut(): void {
    localStorage.removeItem('token');
  }

  /**
   * Get token data decoded, return false when an error occurs
   *
   * @param token
   */
  public getTokenDecoded(token: string): boolean|any {

    if (token == null) {
      return false;
    }

    let result = null;

    try {
      result = jwt_decode(token);
    }
    catch (e) {
      return false;
    }

    return result;
  }
}
