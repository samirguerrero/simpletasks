import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ParamMakerService} from './param-maker.service';
import {UserService} from './user.service';
import {SimpleTaskService} from './simple-task.service';

@Injectable({
  providedIn: 'root'
})
export class MenuOptionService extends SimpleTaskService{
  constructor(private client: HttpClient,
              private paramMakerService: ParamMakerService,
              private userService: UserService) {
    super(client, paramMakerService, userService);
    this.classType = 'menuoption';
  }
}
