import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParamMakerService {

  constructor() { }

  /**
   * Create a json object given an object
   *
   * @param object Object to transform
   */
  getParams(object: any): HttpParams {
    const json = {'json': JSON.stringify(object)};
    return new HttpParams({fromObject: json});
  }
}
