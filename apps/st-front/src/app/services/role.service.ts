import {Injectable} from '@angular/core';
import {SimpleTaskService} from './simple-task.service';
import {HttpClient} from '@angular/common/http';
import {ParamMakerService} from './param-maker.service';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends SimpleTaskService{
  constructor(private client: HttpClient,
              private paramMakerService: ParamMakerService,
              private userService: UserService) {
    super(client, paramMakerService, userService);
    this.classType = 'role';
  }
}
