import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {HttpClientModule} from '@angular/common/http';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {SidebarMenuComponent} from './components/sidebar-menu/sidebar-menu.component';
import {MainContentComponent} from './components/main-content/main-content.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './pages/login/login.component';
import {ClarityModule} from '@clr/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SimpleModalComponent} from './components/simple-modal/simple-modal.component';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from './pages/register/register.component';
import {ForgotPasswordComponent} from './pages/forgot-password/forgot-password.component';
import {ForgotPasswordFinishComponent} from './pages/forgot-password-finish/forgot-password-finish.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {UsersAdminComponent} from './pages/users-admin/users-admin.component';
import {TasksComponent} from './pages/tasks/tasks.component';
import {NewTaskComponent} from './pages/new-task/new-task.component';
import {TasksListComponent} from './components/tasks-list/tasks-list.component';
import {TasksBoardComponent} from './components/tasks-board/tasks-board.component';
import {MomentModule} from 'angular2-moment';
import {FilterColumnTasksPipe} from './pipes/filter-column-tasks.pipe';
import {ThreeDotsMenuComponent} from './components/three-dots-menu/three-dots-menu.component';
import {AddColumnModalComponent} from './components/add-column-modal/add-column-modal.component';

const appRoutes: Routes = [
  // home
  { path: '', component: LoginComponent },

  // user
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password-finish/:hash', component: ForgotPasswordFinishComponent },
  { path: 'users-admin', component: UsersAdminComponent },

  // task
  { path: 'task-list', component: TasksComponent },
  { path: 'new-task', component: NewTaskComponent },
  { path: 'update-task/:uuid', component: NewTaskComponent },

  // not found
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    SidebarMenuComponent,
    MainContentComponent,
    LoginComponent,
    SimpleModalComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ForgotPasswordFinishComponent,
    PageNotFoundComponent,
    ProfileComponent,
    UsersAdminComponent,
    TasksComponent,
    NewTaskComponent,
    TasksListComponent,
    TasksBoardComponent,
    FilterColumnTasksPipe,
    ThreeDotsMenuComponent,
    AddColumnModalComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ClarityModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
