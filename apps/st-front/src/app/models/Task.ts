import {Tag} from './Tag';
import {Column} from './Column';
import {User} from './User';
import {Project} from './Project';

export class Task {

  public uuid: string;
  public title: string;
  public summary: string;
  public description: string;
  public attached: string;
  public expirationDate: string;
  public priority: number;
  public assignment: User;
  public tag: Tag;
  public column: Column;
  public project: Project;

  constructor() {}
}
