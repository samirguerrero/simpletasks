export class Column {

  public uuid: string;
  public title: string;
  public color: string;
  public position: number;

  constructor() {}
}
