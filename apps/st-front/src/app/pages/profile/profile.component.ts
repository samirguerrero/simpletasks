import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RegisterComponent} from '../register/register.component';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {UploaderService} from '../../services/uploader.service';
import {Errors} from '../../helpers/Errors';
import {Subscription} from 'rxjs';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends RegisterComponent implements OnInit, OnDestroy {

  backBasePath: string;
  // different errors can occurs
  errorType: number = -1;
  // when is remove button pulsed
  isRemove: boolean = false;

  // subscriptions array
  subscriptions: Subscription[];

  /** Modal to show messages **/
  @ViewChild("simpleModal", null) simpleModal;

  constructor(private profileFormBuilder: FormBuilder,
              private profileUserService: UserService,
              private profileRouter: Router,
              profileUploadService: UploaderService) {
    super(profileFormBuilder, profileUserService, profileRouter, profileUploadService);

    // init data
    this.user.password = null;
    this.userPass2 = null;
    this.backBasePath = environment.backBasePath;
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {
    // get user id from token
    this.user.uuid =
      this.profileUserService.getTokenDecoded(
        localStorage.getItem('token')
      ).sub;

    // then get other data from server
    const sub = this.profileUserService.show(this.user).subscribe(
      (response) => {
        this.errorType = -1;
        // get json data and set it to user data
        this.user = response.result;
        this.f().um.value = this.user.mail;
        this.f().un.value = this.user.name;
      },
      error => {
        this.errorType = 0;
        const message =
          error.error && error.error.message ?
            error.error.message :
            Errors.GENERAL_ERROR;
        // show the error
        this.openErrorModal(message);
      }
    );

    this.subscriptions.push(sub);

    // form validators
    this.form = this.profileFormBuilder.group({
      um: ['', [
        Validators.email,
        Validators.maxLength(255)
      ]],
      un: ['', [
        Validators.maxLength(75)
      ]],
      up: ['', [
        Validators.minLength(4),
        Validators.maxLength(32)
      ]],
      up_2: ['', [
        Validators.minLength(4),
        Validators.maxLength(32)
      ]],
      av: ['']
    });
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Submit method
   *
   * @param buttonType save or remove button
   */
  onSubmitProfile(buttonType): void {

    this.submitted = true;

    // if is an invalid form then exit let the view show the errors
    if (this.form.invalid) {
      return;
    }

    if (buttonType == 'save') {
      this.saveProfile();
    }

    if (buttonType == 'remove') {
      this.removeAccount();
    }
  }

  /**
   * Save profile if something changes
   */
  private saveProfile(): void {
    this.isRemove = false;
    // get tam password
    const passModified = this.f().up.value.trim().length;

    // only check passwords when old password isn't empty
    if (passModified > 0) {
      // if password mismatch then exit (in the process user.password is set it)
      if (!this.checkPasswordsMatch()) {
        return;
      }
    }

    // get the rest fields
    this.user.mail = this.f().um.value.trim();
    this.user.name = this.f().un.value.trim();

    // then send it to backend
    const sub = this.profileUserService.edit(this.user).subscribe(
      () => {
        this.openSuccessProfileModal('User saved');
      },
      error => {

        const message =
          error && error.error.message ?
            error.error.message :
            Errors.GENERAL_ERROR;
        // show the error
        this.openErrorModal(message);
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Remove method open a warning dialog
   */
  private removeAccount(): void {
    this.isRemove = true;
    this.openWarningModal();
  }

  /**
   * Open success modal
   *
   * @param text string  text to show
   */
  private openSuccessProfileModal(text: string): void{
    this.simpleModal.modal_title = 'Success';
    this.simpleModal.modal_text = text;
    this.simpleModal.modal_button_text = 'Dismiss!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a warning dialog
   */
  private openWarningModal(): void{
    this.simpleModal.modal_title = 'Warning';
    this.simpleModal.modal_text = 'Are you sure to remove this account?';
    this.simpleModal.modal_button_text = 'Yes!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = true;
    this.simpleModal.is_cancellable = true;

    this.simpleModal.openModal();
  }

  /**
   * Close an open dialog,
   * if it's a success dialog then go to task list
   *
   * @return boolean if it's cancelled
   */
  closeModalProfile(is_cancel: boolean): void {
    this.submitted = false;

    // if is cancel button pressed then exit
    if (is_cancel) {
      return;
    }

    // check if is a remove action
    if (!this.simpleModal.is_error && this.isRemove) {
      const sub = this.profileUserService.remove(this.user).subscribe(
        () => {
          localStorage.removeItem('token');
          this.profileRouter.navigate(['/']).then(() => []);
        },
        error => {
          // show the error
          this.openErrorModal(error.error.message);
        }
      );

      this.subscriptions.push(sub);
      return;
    }

    // if is an error exit
    if (this.simpleModal.is_error) {
      // if is a load data error try to reload
      if (this.errorType === 0) {
        location.reload();
      }
      return;
    }
  }

  /**
   * Aux method to sort form controls calls
   */
  protected f(): any {
    return this.form.controls;
  }
}
