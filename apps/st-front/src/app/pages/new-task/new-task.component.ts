import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Task} from '../../models/Task';
import {UploaderService} from '../../services/uploader.service';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Column} from '../../models/Column';
import {Project} from '../../models/Project';
import {Tag} from '../../models/Tag';
import {TagsService} from '../../services/tags.service';
import {ColumnService} from '../../services/column.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskService} from '../../services/task.service';
import {Strings} from '../../helpers/Strings';
import {Cons} from '../../helpers/Cons';
import {ProjectService} from '../../services/project.service';
import {Errors} from '../../helpers/Errors';
import {Subscription} from 'rxjs';
import {environment} from '../../../environments/environment';
import Timer = NodeJS.Timer;

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit, OnDestroy {

  // page title
  title: string;

  // page form
  form: FormGroup;

  fileUrl: string;

  // form submit status to show errors
  submitted: boolean;

  // if the current action is an update or a create action
  isUpdate: boolean = false;

  // it there is an error retrieving data from the server
  retrievingDataError: boolean;

  // main task
  task: Task;

  // attached file selected in the form
  attached: File;

  // priorities array
  priorities: string[];

  // columns array
  columns: Column[];

  // timeouts to search in the server when user typing
  assignmentTimeout: Timer;
  tagTimeout: Timer;

  // suggestions obtained from the server
  suggestionsTagList: Tag[];
  suggestionsUserList: User[];

  // subscriptions array
  subscriptions: Subscription[];

  /** Modal to show messages **/
  @ViewChild('simpleModal', null) simpleModal;

  constructor(
    private taskService: TaskService,
    private uploadService: UploaderService,
    private userService: UserService,
    private tagService: TagsService,
    private columnService: ColumnService,
    private projectService: ProjectService,
    private router: Router,
    private route: ActivatedRoute,
    private el: ElementRef,
    private formBuilder: FormBuilder) {

    // init task object
    this.task = new Task();
    this.task.title = '';
    this.task.summary = '';
    this.task.description = '';
    this.task.attached = null;
    this.task.assignment = new User();
    this.task.assignment.name='';
    this.task.column = new Column();
    this.task.project = new Project();
    this.task.tag = new Tag();
    this.task.tag.title = '';
    this.task.priority = 0;

    // other var init
    this.submitted = false;
    this.retrievingDataError = false;
    this.attached = null;
    this.priorities = [
      'Low', 'Medium', 'High', 'Maximum'
    ];

    // arrays init
    this.columns = [];
    this.suggestionsUserList = [];
    this.suggestionsTagList = [];
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {

    // check if is new task or update task
    let currentUrl = this.router.url;
    currentUrl = currentUrl.substr(0,currentUrl.lastIndexOf('/') + 1);
    this.isUpdate = currentUrl == Cons.frontUpdateTaskPath;

    // select new or update title
    this.title =
      this.isUpdate ? Strings.UPDATE_TASK_PAGE_TITLE : Strings.NEW_TASK_PAGE_TITLE;

    // init form controls
    this.form = this.formBuilder.group({
      title: ['', [
        Validators.required,
        Validators.maxLength(75)
      ]],
      summary: ['', [
        Validators.maxLength(200)
      ]],
      description: [''],
      attached: [''],
      assignment: [''],
      exp_date: [''],
      column: [''],
      tag: [''],
      priority: ['']
    });

    // retrieve main data
    this.getAllColumns();
    this.getAllProjects();

    // if is update get task data from the server
    if (this.isUpdate) {
      this.task.uuid = this.route.snapshot.paramMap.get('uuid');
      const sub = this.taskService.show(this.task).subscribe(
        res => {
          // get the task data
          let task: Task = res.result;

          // if some data comes empty then init them
          if (task.assignment == null) {
            task.assignment = new User();
            task.assignment.name = '';
          }

          if (task.column == null) {
            task.column = new Column();
            task.column.title = '';
          }

          if (task.tag == null) {
            task.tag = new Tag();
            task.tag.title = '';
          }

          if (task.project == null) {
            task.project = new Project();
            task.project.title = '';
          }

          if (task.tag.title != '') {
            this.suggestionsTagList = [
              task.tag
            ];
          }

          if (task.assignment.name != '') {
            this.suggestionsUserList = [
              task.assignment
            ];
          }

          // assign task
          this.task = task;

          // init form controls
          this.f().title.setValue(this.task.title);
          this.f().summary.setValue(this.task.summary);
          this.f().description.setValue(this.task.description);
          this.f().assignment.setValue(this.task.assignment.name);
          this.f().priority.setValue(this.task.priority);
          this.f().column.setValue(this.task.column.title);
          this.f().tag.setValue(this.task.tag.title);

          this.fileUrl = environment.backBasePath + "/uploads/" + this.task.attached.split('/').pop();
        },
        error => {
          this.openErrorModal(error.error.message);
        }
      );

      this.subscriptions.push(sub);
    }
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  // UI METHODS /////////////////////////////////////

  /**
   * On submit form
   *
   * @param buttonType string type of button pressed
   */
  onSubmit(buttonType: string = 'save'): void {
    // change submit status
    this.submitted = true;

    // if is invalid then focus wrong input and exit
    if (this.form.invalid) {

      const formGroupInvalid: NodeList = this.el.nativeElement.querySelectorAll('.ng-invalid');

      if (formGroupInvalid && formGroupInvalid.length > 1) {
        const input = <HTMLInputElement>formGroupInvalid[1];
        input.focus();
      }

      return;
    }

    // if the user wants to delete the task a warning dialog is shown first
    if (buttonType == 'remove') {
      this.openWarningModal();
      return;
    }

    // get form params data
    this.task.title = this.f().title.value.trim();
    this.task.description = this.f().description.value.trim();
    this.task.summary = this.f().summary.value.trim();
    this.task.expirationDate = this.f().exp_date.value.trim();
    const assignment = this.f().assignment.value.trim();
    const tag = this.f().tag.value.trim();


    // check if there is an user selected and then check if is a valid user
    if (assignment != '') {

      let userFounded  =
        this.suggestionsUserList.some( suggestionUser  => {

        let found = suggestionUser.name == assignment;

        if (found) {
          this.task.assignment = suggestionUser;
        }

        return found;
      });

      // if not found a valid user then show the error and exit
      if (!userFounded) {
        this.openErrorModal(Errors.USER_ASSIGNMENT_ERROR);
        return;
      }

    }

    // check if column is set
    if (this.task.column.uuid == '') {
      this.openErrorModal(Errors.COLUMN_NOT_SET_ERROR);
      return;
    }

    // check if tag is set and then check if is a new tag or is an exists task,
    // if is a new tag then exit and finish when new task request ends,
    // @see saveTag method
    if (tag != '' && this.checkTagNotFound(tag)) {
      this.task.tag.color = '#FFF';
      this.task.tag.title = tag;
      this.saveTag(this.task.tag);
      return;
    }

    // if is update task path call to update method
    if (this.isUpdate) {
      this.updateTask();
      return;
    }

    // else, call save method
    this.saveTask();

    return;
  }

  /**
   * Call it from column select like onChange method
   *
   * @param value event.target.value
   */
  changeColumn(value: number): void {
    this.task.column = this.columns[value];
  }

  /**
   * Call it from file selector when a file is choose
   *
   * @param event Angular event for this input
   *              that contains the selected file,
   *              this method upload the file and set
   *              the full path in the server
   */
  fileChange(event): void {

    // when an user select an avatar try to upload it
    if (event.target.files.length > 0) {
      const formData = new FormData();
      const file = event.target.files[0] as File;

      // first check if is allowed file type
      const allowedMimeType = [
        'application/zip'
      ];
      if (allowedMimeType.indexOf(file.type) == -1) {
        this.task.attached = null;
        this.attached = null;
        return;
      }

      // post file to uploader service
      formData.append('uploadFile', file);
      const sub = this.uploadService.upload(formData, false).subscribe(
        (res) => {
          // set file path
          if (res.body) this.task.attached = res.body.message;
        },
        () => {
          // reset data and open error modal
          this.task.attached = null;
          this.attached = null;
          this.openErrorModal(Errors.UPLOAD_FILE_ERROR);
        }
      );

      this.subscriptions.push(sub);
    }
  }

  /**
   * Set priority value from the ui form control
   *
   * @param value event.target.value
   */
  changePriority(value: number): void {
    this.task.priority = value;
  }

  /**
   * Search user suggestions while typing in the form control
   *
   * @param value event.target.value
   */
  changeAssignment(value: string): void {

    // if exists then clear it
    if (this.assignmentTimeout) {
      clearTimeout(this.assignmentTimeout);
    }

    // set timeout to search suggestions in the server
    // to avoid lots of calls to the server
    this.assignmentTimeout =
      setTimeout(() => {
      this.searchAssignment(value)
    },600);

  }

  /**
   * Search tag suggestions while typing in the form control
   *
   * @param value
   */
  changeTag(value: string): void {

    // if exists cancel it
    if (this.tagTimeout) {
      clearTimeout(this.tagTimeout);
    }

    // search tag suggestions when the user stops typing
    this.tagTimeout =
      setTimeout(() => {
      this.searchTagByTitle(value)
    },600);
  }

  // PRIVATE METHODS /////////////////////////////////////

  /**
   * Retrieve all columns data and set task column
   * with the first row if is a new one
   */
  private getAllColumns(): void {
    const sub = this.columnService.list().subscribe(
      res => {

        // initial check
        if (!res.result) {
          this.openErrorModal(Errors.GENERAL_ERROR);
          this.retrievingDataError = true;
          return;
        }

        if (res.result.length == 0) {
          this.openErrorModal(Errors.NO_COLUMNS_ERROR);
          this.retrievingDataError = true;
          return;
        }

        this.columns = res.result;

        // if is a new one then set first column by default
        if (this.columns.length > 0 && !this.isUpdate) {
          this.task.column = this.columns[0];
        }

      },
      error => {
        this.openErrorModal(error.error.message);
        this.retrievingDataError = true;
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Retrieve all projects data and set task project
   * with the first row by default
   */
  private getAllProjects(): void {
    const sub = this.projectService.list().subscribe(
      res => {

        // initial check
        if (!res.result || res.result.length == 0) {
          this.openErrorModal(Errors.GENERAL_ERROR);
          this.retrievingDataError = true;
          return;
        }

        this.task.project = res.result[0];

      },
      error => {
        this.openErrorModal(error.error.message);
        this.retrievingDataError = true;
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Check if the tag is a new tag or is a selected tag
   *
   * @return boolean
   */
  private checkTagNotFound(tagName: string = ''): boolean {
    return !this.suggestionsTagList.some(suggestionTag => {
      const found = suggestionTag.title == tagName;
      if (found) {
        this.task.tag = suggestionTag;
      }
      return found;
    });
  }

  /**
   * Search users in the server by name
   *
   * @param value user name to search
   */
  private searchAssignment(value: string): void {

    // if there is an empty then exit to avoid unnecessary calls
    if (value == '') {
      return;
    }

    const sub = this.userService.searchByName(value).subscribe(
      response => {
        // @ts-ignore
        this.suggestionsUserList = response.result;
      },
      error => {
        this.openErrorModal(error.error.message);
      });

    this.subscriptions.push(sub);
  }

  /**
   * Search tags in the server by title
   *
   * @param value tag title to search
   */
  private searchTagByTitle(value: string): void {

    // if there is an empty string then exit to avoid unnecessary calls
    if (value.trim() == '') {
      return;
    }

    const sub = this.tagService.searchByTitle(value).subscribe(
      response => {
        // @ts-ignore
        this.suggestionsTagList = response.result;

      },
      error => {
        this.openErrorModal(error.error.message);
      });

    this.subscriptions.push(sub);
  }

  /**
   * Save a tag in the server,
   * this method continues submit method
   * that is delayed because
   * if exists a new tag
   * the server expects the tag uuid
   *
   * @param tag Tag to save
   */
  private saveTag(tag: Tag): void {
    const sub = this.tagService.create(tag).subscribe(
      (response) => {

      this.task.tag = response.tag;

      // call update task
      if (this.isUpdate) {
        this.updateTask();
        return;
      }

      // else save task
      this.saveTask();
    },
    error => {
      this.openErrorModal(error.error.message);
    });

    this.subscriptions.push(sub);
  }

  /**
   * Save a task in the server
   */
  private saveTask(): void {
    const sub = this.taskService.create(this.task).subscribe(
      () => {
         this.openSuccessModal(Strings.TASK_CREATED_MESSAGE);
      },
      error => {
        this.openErrorModal(error.error.message)
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Update current task in the server
   */
  private updateTask(): void {
    const sub = this.taskService.edit(this.task).subscribe(
      () => {
        this.openSuccessModal(Strings.TASK_UPDATED_MESSAGE);
      },
      error => {
        this.openErrorModal(error.error.message)
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Aux method to sort form controls calls
   */
  private f(): any {
    return this.form.controls;
  }

  // MODAL METHODS /////////////////////////////////////

  /**
   * Show a success dialog
   */
  private openSuccessModal(text): void{
    this.simpleModal.modal_title = 'Success';
    this.simpleModal.modal_text = text;
    this.simpleModal.modal_button_text = 'Dismiss!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  private openErrorModal(errorMsg: string): void{
    errorMsg = errorMsg ? errorMsg : 'Retrieving data error';
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a warning dialog
   */
  private openWarningModal(): void{
    this.simpleModal.modal_title = 'Warning';
    this.simpleModal.modal_text = Strings.TASK_DELETE_DIALOG_MESSAGE;
    this.simpleModal.modal_button_text = 'Yes!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = true;
    this.simpleModal.is_cancellable = true;

    this.simpleModal.openModal();
  }

  /**
   * Checks if there is an error retrieving
   * data from server or is a remove action
   */
  closeModal($isCancelled: boolean): void {

    // do nothing
    if ($isCancelled) {
      return;
    }

    // error manage
    if (this.simpleModal.is_error && this.retrievingDataError) {
      this.router.navigate([Cons.frontTaskListPath]).then(() => {});
      return;
    }

    // remove action
    if (this.simpleModal.is_warning) {

      const sub = this.taskService.remove(this.task).subscribe(
        () => {
          // navigate to task list
          this.router.navigate([Cons.frontTaskListPath]).then(() => {});
        },
        error => {
          setTimeout(() => this.openErrorModal(error.message), 500);
        }
      );

      this.subscriptions.push(sub);
      return;
    }

    // success
    if (!this.simpleModal.is_error) {
      this.router.navigate([Cons.frontTaskListPath]).then(() => {});
      return;
    }
  }
}
