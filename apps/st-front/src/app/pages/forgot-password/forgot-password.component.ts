import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {Strings} from '../../helpers/Strings';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy{

  // Form controls
  form: FormGroup;
  mail: string;

  // Submitted control
  submitted: boolean;

  // subscriptions array
  subscriptions: Subscription[];

  // Modal to show messages
  @ViewChild("simpleModal", null) simpleModal;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router) {
    this.mail = '';
    this.subscriptions = [];
  }

  /**
   * @inheritDoc
   */
  ngOnInit() {
    // init form validators
    this.form = this.formBuilder.group({
      um: ['', [Validators.required, Validators.email]]
    });
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Send a forgot mail,
   * call it from the ui
   */
  sendMail(): void {
    // change submitted status, this is used in the view to show or not errors
    this.submitted = true;

    // if is an invalid form then exit
    if (this.form.invalid) {
      return;
    }

    // take form param values
    this.mail = this.form.controls.um.value;

    const sub = this.userService.forgotSendEmail(this.mail).subscribe(
      () => {
        // show success dialog yo go login path
        this.openSuccessModal();
      },
      error => {
        // show error
        this.openErrorModal(error.error.message);
      }
    );

    this.subscriptions.push(sub);
  }

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  private openErrorModal(errorMsg: string): void{
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a success dialog
   */
  private openSuccessModal(): void{
    this.simpleModal.modal_title = 'Success';
    this.simpleModal.modal_text = Strings.FORGOT_MAIL_MESSAGE;
    this.simpleModal.modal_button_text = 'Go to login!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Close modal an if it's a success modal then go to login
   */
  closeModal(): void {
    // reset submitted status
    this.submitted = false;

    // check if is an error to go to login
    if (!this.simpleModal.is_error) {
      this.router.navigate(['/']).then(() => {});
    }
  }
}
