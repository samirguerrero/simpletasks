import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TaskService} from '../../services/task.service';
import {Task} from '../../models/Task';
import {Router} from '@angular/router';
import {Cons} from '../../helpers/Cons';
import {Column} from '../../models/Column';
import {ColumnService} from '../../services/column.service';
import {Errors} from '../../helpers/Errors';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

  // task list or board view mode flag
  viewModeBoard: boolean;
  // tasks to show
  tasks: Task[];
  // columns in board mode
  columns: Column[];
  // selected column to remove
  columnToRemove: Column;

  // subscriptions array
  subscriptions: Subscription[];

  // tasks and column count to show a message or the tasks
  tasksCount: number;
  columnsCount: number;

  // Modal view
  @ViewChild('simpleModal', null) simpleModal;

  constructor(
    private taskService: TaskService,
    private columnService: ColumnService,
    private router: Router) {

    this.viewModeBoard = true;
    this.tasks = [];
    this.subscriptions = [];
    this.tasksCount = 0;
    this.columnsCount = 0;
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {
    // get tasks to show
    const taskSub = this.taskService.list().subscribe(
      res => {
        this.tasks = res.result;
        this.tasksCount = this.tasks.length;
      },
      error => {
        this.openErrorModal(error.error.message);
      }
    );

    this.subscriptions.push(taskSub);

    // get columns to show
    const colSub = this.columnService.list().subscribe(
      res => {
        this.columns = res.result;
        this.columnsCount = this.columns.length;
      },
      error => {
        this.openErrorModal(error.error.message);
      }
    );

    this.subscriptions.push(colSub);

    // select initial button state
    const boardBtn = $('#boardBtn');
    boardBtn.addClass('active');
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    // prevent memory leaks
    this.subscriptions.forEach(sub => { sub.unsubscribe()});
  }

  /**
   * Click event to show board mode,
   * called from the ui
   */
  boardModeClick(): void {
    this.viewModeBoard = true;

    const boardBtn = $('#boardBtn');
    if (!boardBtn.hasClass('active')) {
      boardBtn.addClass('active');
    }

    const listBtn = $('#listBtn');
    if (listBtn.hasClass('active')) {
      listBtn.removeClass('active');
    }
  }

  /**
   * Click event to show list mode,
   * called from the ui
   */
  listModeClick(): void {
    this.viewModeBoard = false;

    const listBtn = $('#listBtn');
    if (!listBtn.hasClass('active')) {
      listBtn.addClass('active');
    }

    const boardBtn = $('#boardBtn');
    if (boardBtn.hasClass('active')) {
      boardBtn.removeClass('active');
    }
  }

  /**
   * Navigate to selected task, called from ui
   *
   * @param task tasks to show
   */
  goToTask(task: Task): void {
    this.router.navigate([Cons.frontUpdateTaskPath + task.uuid]).then(() => {});
  }

  /**
   * Change tasks column in the server, called from ui
   *
   * @param task task to move
   */
  updateTaskColumn(task: Task): void {
    const sub = this.taskService.edit(task).subscribe(
      () => { },
      error => {
        this.openErrorModal(error.error.message);
      });

    this.subscriptions.push(sub);
  }

  /**
   * Remove column clicked from column
   *
   * @param column Column to remove
   */
  removeColumnClicked(column: Column): void {
    this.columnToRemove = column;
    this.openWarningModal();
  }

  // MODAL METHODS /////////////////////////////////////

  /**
   * Show an error dialog with a given message
   *
   * @param errorMsg string with an error text
   */
  private openErrorModal(errorMsg: string): void{
    errorMsg = errorMsg ? errorMsg : 'Retrieving data error';
    this.simpleModal.modal_title = 'Error';
    this.simpleModal.modal_text = errorMsg;
    this.simpleModal.modal_button_text = 'Dismiss';
    this.simpleModal.is_error = true;
    this.simpleModal.is_warning = false;
    this.simpleModal.is_cancellable = false;

    this.simpleModal.openModal();
  }

  /**
   * Show a warning dialog
   */
  protected openWarningModal(): void{
    this.simpleModal.modal_title = 'Warning';
    this.simpleModal.modal_text = 'Are you sure to remove this column?';
    this.simpleModal.modal_button_text = 'Yes!';
    this.simpleModal.is_error = false;
    this.simpleModal.is_warning = true;
    this.simpleModal.is_cancellable = true;

    this.simpleModal.openModal();
  }

  /**
   * Checks if there is an error retrieving
   * data from server or is a remove action
   *
   * @param isCancelled boolean
   */
  closeModal(isCancelled: boolean): void {

    // do nothing
    if (isCancelled) {
      return;
    }

    // remove column action
    if (this.simpleModal.is_warning) {

      // check if there are some tasks in the column
      const lisColSub =
        this.taskService.listByColumn(this.columnToRemove)
        .subscribe(
        res => {
          // if there are tasks in the selected column  show an error
          if (res.result && res.result.length > 0) {
            setTimeout(() => {
              this.openErrorModal(Errors.TASKS_IN_COLUMN_ERROR);
            },500);
            return;
          }

          // else proceed to remove this column
          const colRemoveSub =
            this.columnService.remove(this.columnToRemove).subscribe(
            () => {
              // if works then remove from the ui
              this.columns.splice(this.columns.indexOf(this.columnToRemove),1);

            },
            error => {
              setTimeout(() => {
                this.openErrorModal(error.error.message);
              },500);
            }
          );

          this.subscriptions.push(colRemoveSub);

        },
        error => {
          setTimeout(() => {
            this.openErrorModal(error.error.message);
          },500);
        }
      );

      this.subscriptions.push(lisColSub);
    }
  }
}
